resource "google_project_service" "app-engine" {
  project = var.project_id
  disable_dependent_services = false
  service = "appengine.googleapis.com"
}

resource "google_project_service" "compute" {
  project = var.project_id
  disable_dependent_services = false
  service = "compute.googleapis.com"
}

resource "google_project_service" "deployment-manager" {
  project = var.project_id
  disable_dependent_services = false
  service = "deploymentmanager.googleapis.com"
}

resource "google_project_service" "functions" {
  project = var.project_id
  disable_dependent_services = false
  service = "cloudfunctions.googleapis.com"
}

resource "google_project_service" "logging" {
  project = var.project_id
  disable_dependent_services = false
  service = "logging.googleapis.com"
}

resource "google_project_service" "monitoring" {
  project = var.project_id
  disable_dependent_services = false
  service = "monitoring.googleapis.com"
}

resource "google_project_service" "secretmanager" {
  project = var.project_id
  disable_dependent_services = false
  service = "secretmanager.googleapis.com"
}
