##  Terraform 
terraform {
  backend "gcs" {
    bucket = "my-state-bucket"
    prefix = "bootstrap"
  }
  required_version = ">= 0.14"
  required_providers {
    aws = { 
      source  = "hashicorp/aws"
      version = "~> 3.25"
    }   
  }
}


provider "google" {
  alias = "usw2a"
  billing_project = "my-billing-project"
  region = "us-west2"
  zone = "us-west2-a"
  project = "my-project"
}
