resource "google_project" "project" {
  name       = var.project_name
  org_id     = var.org_id
  project_id = var.project_id
  billing_account = var.billing_account
}
