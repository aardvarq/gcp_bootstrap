##  Terraform 
terraform {
  required_version = ">= 0.14"
  required_providers {
    aws = { 
      source  = "hashicorp/aws"
      version = "~> 3.25"
    }   
  }
}

provider "google" {
  project = "my-project"
  region = "us-west2"
  zone = "us-west2-a"
}

resource "google_storage_bucket" "tfstate" {
  name = "my-state-bucket"
  location = "US"
  force_destroy = false
  versioning {
    enabled = true
  }
}
